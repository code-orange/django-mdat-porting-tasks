import csv

from celery import shared_task

from django_mdat_location.django_mdat_location.models import MdatCities, MdatCountries
from django_mdat_porting.django_mdat_porting.models import MdatPortingPartners


@shared_task(name="mdat_porting_sync_from_ekp_portal_tkg46_list")
def mdat_porting_sync_from_ekp_portal_tkg46_list():
    with open("data/tkg46-ekp-20210415220000.csv") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=";")

        for row in csv_reader:
            # only import entry including itu_carrier_code and auth_reg_id
            if row[0] in (None, "") or row[1] in (None, ""):
                continue

            row_auth_reg_id = str(row[0]).strip()
            row_itu_carrier_code = str(row[1]).strip()
            row_name = str(row[3]).strip()

            try:
                porting_carrier = MdatPortingPartners.objects.get(
                    auth_reg_id=row_auth_reg_id,
                    itu_carrier_code=row_itu_carrier_code,
                )
            except MdatPortingPartners.DoesNotExist:
                porting_carrier = MdatPortingPartners(
                    auth_reg_id=row_auth_reg_id,
                    itu_carrier_code=row_itu_carrier_code,
                )

            porting_carrier.name = row_name
            porting_carrier.save()

    return


@shared_task(name="mdat_porting_sync_from_ekp_portal_company_export")
def mdat_porting_sync_from_ekp_portal_company_export():
    with open(
        "data/diensteanbieter-liste_2021_04_19_csv_export.csv", "rt", encoding="utf-8"
    ) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=";")

        for row in csv_reader:
            if row[6] != "Deutschland":
                continue

            row_auth_reg_id = str(row[0]).strip()

            for porting_partner in MdatPortingPartners.objects.filter(
                auth_reg_id=row_auth_reg_id
            ):
                try:
                    porting_partner.city = MdatCities.objects.get(
                        country=MdatCountries.objects.get(iso="de"),
                        zip_code=str(row[2]).strip(),
                    )
                    porting_partner.street = str(row[4]).strip()
                    porting_partner.house_nr = str(row[5]).strip()
                    porting_partner.save()
                except:
                    continue

    return
