from django.core.management.base import BaseCommand

from django_mdat_porting_tasks.django_mdat_porting_tasks.tasks import (
    mdat_porting_sync_from_ekp_portal_tkg46_list,
)


class Command(BaseCommand):
    help = "Run task mdat_porting_sync_from_ekp_portal_tkg46_list"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        mdat_porting_sync_from_ekp_portal_tkg46_list()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
